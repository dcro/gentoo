EAPI=6

inherit meson

DESCRIPTION="Fractal is a Matrix messaging app for GNOME written in Rust."
HOMEPAGE="https://wiki.gnome.org/Apps/Fractal"
SRC_URI="https://gitlab.gnome.org/GNOME/fractal/uploads/ad6a483327c3e9ef5bb926b89fb26e2b/fractal-4.0.0.tar.xz"

SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"

DEPENDS="
	virtual/rust
	gnome-extra/libhandy
"
