EAPI=6

inherit meson vala

DESCRIPTION="A library full of GTK+ widgets for mobile phone"
HOMEPAGE="https://source.puri.sm/Librem5/libhandy"
SRC_URI="https://source.puri.sm/Librem5/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2"

SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"

DEPEND="$(vala_depend)"

S="${WORKDIR}/${PN}-v${PV}"
EMESON_SOURCE="${S}"
BUILD_DIR="${S}/${P}"

src_prepare() {
	eapply_user
	vala_src_prepare
}
